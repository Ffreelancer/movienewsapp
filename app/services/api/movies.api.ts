import { Api } from "."
export const API_KEY = "4d33ef4ede6267f0f1676bef4933d694"
export const IMAGE_URL = "https://image.tmdb.org/t/p/original"

export const fetchDiscovery = async (api: Api, page: any) => {
  const discovery = await api.apisauce.get(
    `/discover/movie?api_key=${API_KEY}&sort_by=popularity.desc&include_video=true&page=${page}`,
  )
  return discovery.data
}

export const fetchUpcoming = async (api: Api, page: any) => {
  const upcoming = await api.apisauce.get(`/movie/upcoming?api_key=${API_KEY}&page=${page}`)
  return upcoming.data
}

export const fetchPopular = async (api: Api, page: any) => {
  const popular = await api.apisauce.get(`/movie/popular?api_key=${API_KEY}&page=${page}`)
  return popular.data
}

export const fetchTopRated = async (api: Api, page: any) => {
  const top = await api.apisauce.get(`/movie/top_rated?api_key=${API_KEY}&page=${page}`)
  return top.data
}

export const fetchMovieDetails = async (api: Api, idMovie: any) => {
  const movieDetails = await api.apisauce.get(`/movie/${idMovie}?api_key=${API_KEY}&language=en-US`)
  return movieDetails.data
}

export const fetchMovieCredit = async (api: Api, idMovie: any) => {
  const credits = await api.apisauce.get(
    `/movie/${idMovie}/credits?api_key=${API_KEY}&language=en-US`,
  )
  return credits.data
}

export const fetchMovieReview = async (api: Api, idMovie: any) => {
  const reviews = await api.apisauce.get(
    `/movie/${idMovie}/reviews?api_key=${API_KEY}&language=en-US&page=1`,
  )
  return reviews.data
}

export const fetchSimilarMovies = async (api: Api, idMovie: any, page: any) => {
  const similars = await api.apisauce.get(
    `/movie/${idMovie}/similar?api_key=${API_KEY}&language=en-US&page=${page}`,
  )
  return similars.data
}

export const searchMovieByQuery = async (api: Api, query: any, page: any) => {
  const searchedMovie = await api.apisauce.get(
    `/search/movie?api_key=${API_KEY}&language=en-US&query=${query}&page=${page}`,
  )
  return searchedMovie.data
}

export const fetchLatestMovie = async (api: Api) => {
  const latest = await api.apisauce.get(`/movie/latest?api_key=${API_KEY}&language=en-US`)
  return latest.data
}
