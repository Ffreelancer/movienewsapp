import React, { FC, useEffect } from "react"
import { Alert, Image, TouchableOpacity } from "react-native"
import { IMAGE_URL } from "../../services/api/movies.api"
import { styles } from "./movie-card.style"

type MovieCardProps = {
  onPress(): void
  data: any
}

export const MovieCard: FC<MovieCardProps> = ({ onPress, data }) => {
  return (
    <TouchableOpacity
      onPress={() => {
        onPress()
      }}
      style={styles.cardHolder}
    >
      <Image
        source={
          data.poster_path !== null
            ? { uri: `${IMAGE_URL}${data.poster_path}` }
            : require("../../../assets/images/poster-not-available.jpg")
        }
        resizeMethod={"resize"}
        resizeMode={"cover"}
        style={styles.cover}
      />
    </TouchableOpacity>
  )
}
