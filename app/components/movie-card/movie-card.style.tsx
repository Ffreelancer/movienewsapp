import { Dimensions, StyleSheet } from "react-native"
import { palette } from "../../theme/palette"

const screenWidth = Dimensions.get("screen").width
const screenHeight = Dimensions.get("screen").height

export const styles = StyleSheet.create({
  cardHolder: {
    display: "flex",
    height: 200,
    width: 140,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 20,
    backgroundColor: palette.white,
    overflow: "hidden",
    marginRight: 10,
  },
  cover: {
    height: "100%",
    width: "100%",
  },
})
