import { LinearGradient } from "expo-linear-gradient"
import React, { FC, useEffect } from "react"
import { Alert, Image, Text, TouchableOpacity, View } from "react-native"
import { IMAGE_URL } from "../../services/api/movies.api"
import { palette } from "../../theme/palette"
import { styles } from "./displayer-card.style"

type DisplayerCardProps = {
  onPress(): void
  data: any
  labelText: any
}

export const DisplayerCard: FC<DisplayerCardProps> = ({ onPress, data, labelText }) => {
  return (
    <TouchableOpacity
      onPress={() => {
        onPress()
      }}
      style={styles.cardHolder}
    >
      <Image
        source={
          data.poster_path !== null
            ? { uri: `${IMAGE_URL}${data.poster_path}` }
            : require("../../../assets/images/poster-display.png")
        }
        resizeMethod={"resize"}
        resizeMode={"cover"}
        style={styles.cover}
      />

      <View style={styles.cardLabel}>
        <Text style={styles.labelText}>{labelText}</Text>
      </View>

      <LinearGradient colors={["rgba(0,0,0,.15)", palette.mainLigher]} style={styles.infoHolder}>
        <Text style={styles.movieTitle}>{data.title}</Text>
        <View style={styles.genreHolder}>
          {data.genres.length !== 0
            ? data.genres.map((item: any, idx: any) => (
                <Text style={styles.genre}>{item.name}</Text>
              ))
            : null}
        </View>
        <Text style={styles.movieDate}>{`${data.runtime}min`}</Text>
      </LinearGradient>
    </TouchableOpacity>
  )
}
