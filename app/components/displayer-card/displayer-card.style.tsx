import { Dimensions, StyleSheet } from "react-native"
import { palette } from "../../theme/palette"

const screenWidth = Dimensions.get("screen").width
const screenHeight = Dimensions.get("screen").height

export const styles = StyleSheet.create({
  cardHolder: {
    display: "flex",
    height: screenHeight / 3.5,
    width: screenWidth - 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 20,
    backgroundColor: palette.white,
    overflow: "hidden",
    marginVertical: 25,
  },
  cover: {
    height: "100%",
    width: "100%",
  },
  infoHolder: {
    height: "50%",
    width: "100%",
    paddingHorizontal: 15,
    paddingVertical: 10,
    position: "absolute",
    bottom: 0,
  },
  cardLabel: {
    height: 25,
    width: "auto",
    paddingHorizontal: 15,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: palette.main,
    position: "absolute",
    top: 10,
    right: 10,
    borderRadius: 10,
  },
  labelText: {
    fontSize: 10,
    color: palette.white,
  },
  movieTitle: {
    color: palette.white,
    fontSize: 14,
  },
  genreHolder: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  genre: {
    fontSize: 12,
    color: palette.white,
    marginRight: 7.5,
  },
  movieDate: {
    fontSize: 12,
    color: palette.white,
  },
})
