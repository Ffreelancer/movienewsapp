import { faAngleRight } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import { useNavigation } from "@react-navigation/core"
import React, { FC, useState } from "react"
import { FlatList, View, Text, TouchableOpacity } from "react-native"
import { palette } from "../../theme/palette"
import { MovieCard } from "../movie-card"
import { styles } from "./movies-list.style"

type MoviesListProps = {
  name: string
  apiIndicator: string
  data: any[]
}

export const MoviesList: FC<MoviesListProps> = ({ name, data, apiIndicator }) => {
  const navigation = useNavigation()
  return (
    <View style={styles.mainContainer}>
      <View style={styles.titleHolder}>
        <Text style={styles.titleStyle}>{name}</Text>
        <TouchableOpacity
          style={styles.moreIcon}
          onPress={() => {
            navigation.navigate(
              "viewMore" as never,
              {
                title: name,
                apiInd: apiIndicator,
              } as never,
            )
          }}
        >
          <FontAwesomeIcon icon={faAngleRight} color={palette.white} size={20} />
        </TouchableOpacity>
      </View>
      <FlatList
        data={data}
        horizontal
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (
          <MovieCard
            data={item}
            onPress={() => {
              console.log("Pressed")
              navigation.navigate(
                "details" as never,
                {
                  movieId: item.id,
                } as never,
              )
            }}
          />
        )}
      />
    </View>
  )
}
