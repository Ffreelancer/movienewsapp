import { StyleSheet } from "react-native"
import { palette } from "../../theme/palette"

export const styles = StyleSheet.create({
  mainContainer: {
    display: "flex",
    marginTop: 15,
    marginBottom: 15,
    paddingLeft: 15,
  },
  titleStyle: {
    fontSize: 20,
    color: palette.white,
  },
  titleHolder: {
    marginBottom: 15,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  moreIcon: {
    height: 30,
    width: 30,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
  },
})
