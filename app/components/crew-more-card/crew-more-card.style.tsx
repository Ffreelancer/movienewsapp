import { Dimensions, StyleSheet } from "react-native"
import { palette } from "../../theme/palette"

const screenWidth = Dimensions.get("screen").width
const screenHeight = Dimensions.get("screen").height

export const styles = StyleSheet.create({
  cardHolder: {
    display: "flex",
    height: screenHeight / 3,
    width: screenWidth / 2 - 20,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 20,
    backgroundColor: palette.white,
    overflow: "hidden",
    marginRight: 7.5,
    marginLeft: 7.5,
    marginBottom: 15,
  },
  cover: {
    height: "100%",
    width: "100%",
  },
  actorNameStyle: {
    fontSize: 12,
    color: palette.white,
  },
  nameHolder: {
    height: "20%",
    width: "100%",
    position: "absolute",
    bottom: 0,
    zIndex: 10,
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
})
