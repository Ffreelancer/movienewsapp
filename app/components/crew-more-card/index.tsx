import { LinearGradient } from "expo-linear-gradient"
import React, { FC, useEffect } from "react"
import { View, Image, Text } from "react-native"
import { IMAGE_URL } from "../../services/api/movies.api"
import { palette } from "../../theme/palette"
import { styles } from "./crew-more-card.style"

type CrewMoreCardProps = {
  data: any
}

export const CrewMoreCard: FC<CrewMoreCardProps> = ({ data }) => {
  return (
    <View style={styles.cardHolder}>
      <Image
        source={
          data.profile_path !== null
            ? { uri: `${IMAGE_URL}${data.profile_path}` }
            : require("../../../assets/images/no-profile-pic.png")
        }
        resizeMethod={"resize"}
        resizeMode={"cover"}
        style={styles.cover}
      />
      <LinearGradient colors={["transparent", palette.mainLigher]} style={styles.nameHolder}>
        <Text style={styles.actorNameStyle}>{data.name}</Text>
      </LinearGradient>
    </View>
  )
}
