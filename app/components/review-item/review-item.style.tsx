import { Dimensions, StyleSheet } from "react-native"
import { palette } from "../../theme/palette"

const screenWidth = Dimensions.get("screen").width
const screenHeight = Dimensions.get("screen").height

export const styles = StyleSheet.create({
  reviewHolder: {
    display: "flex",
    width: "100%",
    flexDirection: "row",
    marginBottom: 25,
  },
  avatar: {
    height: 40,
    width: 40,
    borderRadius: 20,
    marginRight: 10,
  },
  reviewDetails: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    backgroundColor: palette.main,
    padding: 10,
    borderRadius: 10,
  },
  usernameText: {
    fontSize: 14,
    color: palette.white,
    marginBottom: 15,
  },
  commentText: {
    fontSize: 12,
    color: palette.white,
  },
})
