import React, { FC, useEffect } from "react"
import { Alert, Image, TouchableOpacity, View, Text } from "react-native"
import { IMAGE_URL } from "../../services/api/movies.api"
import { styles } from "./review-item.style"

type ReviewCardProps = {
  data: any
}

export const ReviewCard: FC<ReviewCardProps> = ({ data }) => {
  return (
    <View style={styles.reviewHolder}>
      <Image
        source={require("../../../assets/images/avatar.jpg")}
        resizeMethod={"resize"}
        resizeMode={"cover"}
        style={styles.avatar}
      />
      <View style={styles.reviewDetails}>
        <Text style={styles.usernameText}>{data.author}</Text>
        <Text numberOfLines={8} style={styles.commentText}>
          {data.content}
        </Text>
      </View>
    </View>
  )
}
