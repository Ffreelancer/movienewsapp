import React, { FC, useEffect } from "react"
import { Alert, Image, Text, View } from "react-native"
import { IMAGE_URL } from "../../services/api/movies.api"
import { styles } from "./cast-card.style"

type CastCardProps = {
  data: any
}

export const CastCard: FC<CastCardProps> = ({ data }) => {
  return (
    <View style={styles.cardHolder}>
      <Image
        source={
          data.profile_path !== null
            ? { uri: `${IMAGE_URL}${data.profile_path}` }
            : require("../../../assets/images/no-profile-pic.png")
        }
        resizeMethod={"resize"}
        resizeMode={"cover"}
        style={styles.cover}
      />
      <Text style={styles.actorNameStyle}>{data.name}</Text>
    </View>
  )
}
