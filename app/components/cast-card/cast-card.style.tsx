import { Dimensions, StyleSheet } from "react-native"
import { palette } from "../../theme/palette"

const screenWidth = Dimensions.get("screen").width
const screenHeight = Dimensions.get("screen").height

export const styles = StyleSheet.create({
  cardHolder: {
    display: "flex",
    height: 250,
    width: 140,

    overflow: "hidden",
    marginRight: 10,
  },
  cover: {
    height: 200,
    width: "100%",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 20,
    marginBottom: 10,
  },
  actorNameStyle: {
    fontSize: 12,
    color: palette.white,
    textAlign: "center",
  },
})
