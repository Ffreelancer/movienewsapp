import { Dimensions, StyleSheet } from "react-native"
import { palette } from "../../theme/palette"

const screenWidth = Dimensions.get("screen").width
const screenHeight = Dimensions.get("screen").height

export const styles = StyleSheet.create({
  barStyle: {
    width: screenWidth,
    backgroundColor: "transparent",
    display: "flex",
    flexDirection: "row",
    paddingVertical: 15,
    paddingHorizontal: 15,
    alignItems: "center",
    justifyContent: "space-between",
    height: 75,
  },
  menuButtonStyle: {
    backgroundColor: "rgba(255, 255, 255,0.1)",
    height: 40,
    width: 40,
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
  },
  rightSideHolder: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  searchButton: {
    marginRight: 10,
  },
  avatarHolder: {
    height: 40,
    width: 40,
    overflow: "hidden",
  },
  avatarImage: {
    height: 40,
    width: 40,
  },
})
