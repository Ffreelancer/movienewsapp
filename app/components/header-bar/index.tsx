import { faBars, faBurger, faSearch } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import { DrawerActions, useNavigation } from "@react-navigation/core"
import React, { FC } from "react"
import { TouchableOpacity, View, Image } from "react-native"
import { palette } from "../../theme/palette"
import { styles } from "./header-bar.style"

export const HeaderBar: FC = () => {
  const navigation = useNavigation()
  return (
    <View style={styles.barStyle}>
      <>
        <TouchableOpacity
          style={styles.menuButtonStyle}
          onPress={() => {
            navigation.dispatch(DrawerActions.toggleDrawer())
          }}
        >
          <FontAwesomeIcon icon={faBars} size={20} color={palette.white} />
        </TouchableOpacity>
      </>
      <View style={styles.rightSideHolder}>
        <TouchableOpacity style={styles.avatarHolder}>
          <Image
            style={styles.avatarImage}
            source={require("../../../assets/images/user.jpg")}
            resizeMethod={"resize"}
            resizeMode={"cover"}
            borderRadius={25}
          />
        </TouchableOpacity>
      </View>
    </View>
  )
}
