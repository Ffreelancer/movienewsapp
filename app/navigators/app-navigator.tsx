/**
 * The app navigator (formerly "AppNavigator" and "MainNavigator") is used for the primary
 * navigation flows of your app.
 * Generally speaking, it will contain an auth flow (registration, login, forgot password)
 * and a "main" flow which the user will use once logged in.
 */
import React from "react"
import { useColorScheme, View } from "react-native"
import { NavigationContainer, DefaultTheme, DarkTheme } from "@react-navigation/native"
import { createNativeStackNavigator } from "@react-navigation/native-stack"
import { HomeScreen, AuthGuideScreen } from "../screens/index"
import { navigationRef, useBackButtonHandler } from "./navigation-utilities"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { palette } from "../theme/palette"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import {
  faCoffee,
  faHome,
  faLocation,
  faHistory,
  faSearch,
  faHeart,
} from "@fortawesome/free-solid-svg-icons"
import { MovieDetailScreen } from "../screens/movie-detail-screen"
import { SearchScreen } from "../screens/search-screen"
import { HeaderBar } from "../components/header-bar"
import { MovieViewAllScreen } from "../screens/movie-view-all-screen"
import { CastViewAllScreen } from "../screens/cast-view-all-screen"
import { createDrawerNavigator } from "@react-navigation/drawer"
import { FavoriteScreen } from "../screens/favorite-screen"

export type NavigatorParamList = {
  authGuide: undefined
  home: undefined
  details: undefined
  //viewMore: undefined
  //castViewMore: undefined
  // 🔥 Your screens go here
}
export type BottomNavParamList = {
  home: undefined
  search: undefined
  home2: undefined
  home3: undefined
  // 🔥 Your screens go here
}

const Drawer = createDrawerNavigator()

const NavigationDrawer = () => {
  return (
    <Drawer.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Drawer.Screen
        name="homeScreen"
        component={BottomTabNavigator}
        options={{
          drawerLabel: "Home",
        }}
      />
      <Drawer.Screen
        name="viewMore"
        component={MovieViewAllScreen}
        options={{
          drawerLabel: () => null,
          title: null,
          drawerIcon: () => null,
        }}
      />
      <Drawer.Screen
        name="castViewMore"
        component={CastViewAllScreen}
        options={{
          drawerLabel: () => null,
          title: null,
          drawerIcon: () => null,
        }}
      />
    </Drawer.Navigator>
  )
}

const Tab = createBottomTabNavigator()

const BottomTabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          position: "absolute",
          height: 60,
          bottom: 0,
          borderTopLeftRadius: 15,
          borderTopRightRadius: 15,
          backgroundColor: palette.main,
          borderTopWidth: 0,
        },
      }}
    >
      <Tab.Screen
        name="home"
        component={HomeScreen}
        options={{
          tabBarShowLabel: false,
          tabBarIcon: ({ color, size }) => (
            <View style={{ display: "flex", flexDirection: "column" }}>
              <FontAwesomeIcon icon={faHome} color={color} size={20} />
            </View>
          ),
          tabBarActiveTintColor: palette.white,
          tabBarInactiveTintColor: palette.lightGrey,
        }}
      />
      <Tab.Screen
        name="favorite"
        component={FavoriteScreen}
        options={{
          tabBarShowLabel: false,
          tabBarIcon: ({ color, size }) => (
            <View style={{ display: "flex", flexDirection: "column" }}>
              <FontAwesomeIcon icon={faHeart} color={color} size={20} />
            </View>
          ),
          tabBarActiveTintColor: palette.white,
          tabBarInactiveTintColor: palette.lightGrey,
        }}
      />
      <Tab.Screen
        name="search"
        component={SearchScreen}
        options={{
          tabBarShowLabel: false,
          tabBarIcon: ({ color, size }) => (
            <View style={{ display: "flex", flexDirection: "column" }}>
              <FontAwesomeIcon icon={faSearch} color={color} size={20} />
            </View>
          ),
          tabBarActiveTintColor: palette.white,
          tabBarInactiveTintColor: palette.lightGrey,
        }}
      />
    </Tab.Navigator>
  )
}

const Stack = createNativeStackNavigator<NavigatorParamList>()

const AppStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="authGuide"
    >
      <Stack.Screen name="authGuide" component={AuthGuideScreen} />
      <Stack.Screen name="home" component={NavigationDrawer} />
      <Stack.Screen name="details" component={MovieDetailScreen} />
      {/** 🔥 Your screens go here */}
    </Stack.Navigator>
  )
}

interface NavigationProps extends Partial<React.ComponentProps<typeof NavigationContainer>> {}

export const AppNavigator = (props: NavigationProps) => {
  const colorScheme = useColorScheme()
  useBackButtonHandler(canExit)
  return (
    <NavigationContainer
      ref={navigationRef}
      theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
      {...props}
    >
      <AppStack />
    </NavigationContainer>
  )
}

AppNavigator.displayName = "AppNavigator"

const exitRoutes = ["AuthGuideScreen"]
export const canExit = (routeName: string) => exitRoutes.includes(routeName)
