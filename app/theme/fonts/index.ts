import * as Font from "expo-font"

export const initFonts = async () => {
  // Refer to ./assets/fonts/custom-fonts.md for instructions.
  // ...
  // Welcome back! Just uncomment this and replace/append with your font file names!
  // ⬇
  await Font.loadAsync({
    //Roboto: require("./Roboto-Thin.ttf"),
    Gravity: require("./Gravity-UltraLightItalic.otf"),
    "Roboto-Regular": require("./Roboto-Regular.ttf"),
    "Roboto-Bold": require("./Roboto-Bold.ttf"),
    "Roboto-Medium": require("./Roboto-Medium.ttf"),
    "Roboto-Light": require("./Roboto-Light.ttf"),
    "Roboto-Thin": require("./Roboto-Thin.ttf"),
  })
}
