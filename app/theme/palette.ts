export const palette = {
  black: "#1d1d1d",
  white: "#ffffff",
  offWhite: "#e6e6e6",
  main: "#5d3da6",
  mainLigher: "#7657bf",
  mainDarker: "#291b4a",
  lightGrey: "#939AA4",
  lighterGrey: "#CDD4DA",
  angry: "#dd3333",
  deepPurple: "#5D2555",
  pink: "#bb00bd",
}
