import React, { FC } from "react"
import { View, Text } from "react-native"
import { styles } from "./favorite-screen.style"

export const FavoriteScreen: FC = () => {
  return (
    <View style={styles.mainContainer}>
      <Text style={styles.textStyle}>{"No Favorite Movie Available"}</Text>
    </View>
  )
}
