import { StyleSheet } from "react-native"
import { palette } from "../../theme/palette"

export const styles = StyleSheet.create({
  mainContainer: {
    display: "flex",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: palette.mainDarker,
  },
  textStyle: {
    fontSize: 12,
    color: palette.white,
  },
})
