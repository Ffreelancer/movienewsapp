import { Dimensions, StyleSheet } from "react-native"
import { palette } from "../../theme/palette"

const screenWidth = Dimensions.get("screen").width
const screenHeight = Dimensions.get("screen").height

export const styles = StyleSheet.create({
  mainContainer: {
    display: "flex",
    flex: 1,
    backgroundColor: palette.mainDarker,
  },
  genreLabelButton: {
    marginRight: 25,
  },
  genreLabelText: {
    color: palette.white,
    fontSize: 16,
    fontWeight: "400",
    fontFamily: "Roboto-Medium",
  },
  loaderHolder: {
    display: "flex",
    width: "100%",
    height: 100,
    alignItems: "center",
    justifyContent: "center",
  },
})
