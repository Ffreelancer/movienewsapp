import { useNavigation } from "@react-navigation/core"
import React, { FC, useEffect, useState } from "react"
import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native"
import { DisplayerCard } from "../../components/displayer-card"

import { HeaderBar } from "../../components/header-bar"
import { MoviesList } from "../../components/movies-list"
import { Api } from "../../services/api"
import {
  fetchDiscovery,
  fetchLatestMovie,
  fetchPopular,
  fetchTopRated,
  fetchUpcoming,
} from "../../services/api/movies.api"
import { palette } from "../../theme/palette"
import { styles } from "./home-screen.style"

type NAVIGATION_PROPS = {
  screen: string
  param: any
}

export const HomeScreen: FC = () => {
  const navigation = useNavigation()
  const [isDiscoverLoading, setIsDiscoverLoading] = useState<boolean>(false)
  const [isComingSoonLoading, setIsComingSoonLoading] = useState<boolean>(false)
  const [isPopularLoading, setIsPopularLoading] = useState<boolean>(false)
  const [isTopLoading, setIsTopLoading] = useState<boolean>(false)
  const [isLatestLoading, setIsLatestLoading] = useState<boolean>(true)

  const [discoverList, setDiscoverList] = useState<any>([])
  const [upcomingList, setUpcomingList] = useState<any>([])
  const [popularList, setPopularList] = useState<any>([])
  const [topRatedList, setTopRatedList] = useState<any>([])
  const [latestMovie, setLatestMovie] = useState<any>(null)

  const GENRE_ARRAY = [
    {
      name: "All",
      selected: false,
    },
    {
      name: "Action",
      selected: true,
    },
    {
      name: "Comedy",
      selected: false,
    },
    {
      name: "Adventure",
      selected: false,
    },
    {
      name: "Horror",
      selected: false,
    },
    {
      name: "Si-fi",
      selected: false,
    },
    {
      name: "Romance",
      selected: false,
    },
    {
      name: "Mystrey",
      selected: false,
    },
    {
      name: "Drama",
      selected: false,
    },
  ]
  const api = new Api()
  useEffect(() => {
    setIsDiscoverLoading(true)
    fetchDiscovery(api, 1)
      .then((result: any) => {
        setDiscoverList(result.results.slice(0, 10))
        setIsDiscoverLoading(false)
      })
      .catch((err) => {
        console.log("Error While Fetching Discoveries")
      })
  }, [])

  useEffect(() => {
    setIsComingSoonLoading(true)
    fetchUpcoming(api, 1)
      .then((result: any) => {
        setUpcomingList(result.results.slice(0, 10))
        setIsComingSoonLoading(false)
      })
      .catch((err) => {
        console.log("Error While Fetching Upcoming")
      })
  }, [])

  useEffect(() => {
    setIsPopularLoading(true)
    fetchPopular(api, 1)
      .then((result: any) => {
        setPopularList(result.results.slice(0, 10))
        setIsPopularLoading(false)
      })
      .catch((err) => {
        console.log("Error While Fetching Popular")
      })
  }, [])

  useEffect(() => {
    setIsTopLoading(true)
    fetchTopRated(api, 1)
      .then((result: any) => {
        setTopRatedList(result.results.slice(0, 10))
        setIsTopLoading(false)
      })
      .catch((err) => {
        console.log("Error While Fetching Top Rated")
      })
  }, [])

  useEffect(() => {
    setIsLatestLoading(true)
    fetchLatestMovie(api)
      .then((result: any) => {
        console.log(result)
        setLatestMovie(result)
        setIsLatestLoading(false)
      })
      .catch((err) => {
        console.log("Error While Fetching Latest Movie")
      })
  }, [])

  return (
    <SafeAreaView style={styles.mainContainer}>
      <HeaderBar />
      <ScrollView style={{ marginBottom: 60 }}>
        {/* <FlatList
          data={GENRE_ARRAY}
          style={{ flexGrow: 0 }}
          contentContainerStyle={{
            marginLeft: 15,
            height: 50,
            alignItems: "center",
          }}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
          horizontal
          renderItem={({ item, index }) => (
            <TouchableOpacity
              onPress={() => {
                navigation.navigate("details" as never)
              }}
              style={styles.genreLabelButton}
            >
              <Text
                style={[
                  styles.genreLabelText,
                  { color: item.selected ? palette.pink : palette.white },
                ]}
              >
                {item.name}
              </Text>
            </TouchableOpacity>
          )}
        /> */}

        {isLatestLoading ? (
          <View style={styles.loaderHolder}>
            <ActivityIndicator size={"large"} color={palette.main} />
          </View>
        ) : (
          <View
            style={{
              width: "100%",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <DisplayerCard
              labelText={"Latest"}
              data={latestMovie}
              onPress={() => {
                navigation.navigate(
                  "details" as never,
                  {
                    movieId: latestMovie.id,
                  } as never,
                )
              }}
            />
          </View>
        )}

        {isDiscoverLoading ? (
          <View style={styles.loaderHolder}>
            <ActivityIndicator size={"large"} color={palette.main} />
          </View>
        ) : (
          <MoviesList name={"Discovery"} apiIndicator={"discovery"} data={discoverList} />
        )}

        {isPopularLoading ? (
          <View style={styles.loaderHolder}>
            <ActivityIndicator size={"large"} color={palette.main} />
          </View>
        ) : (
          <MoviesList name={"Popular"} apiIndicator={"popular"} data={popularList} />
        )}

        {isTopLoading ? (
          <View style={styles.loaderHolder}>
            <ActivityIndicator size={"large"} color={palette.main} />
          </View>
        ) : (
          <MoviesList name={"Top Rated"} apiIndicator={"top-rated"} data={topRatedList} />
        )}

        {isComingSoonLoading ? (
          <View style={styles.loaderHolder}>
            <ActivityIndicator size={"large"} color={palette.main} />
          </View>
        ) : (
          <MoviesList name={"Coming Soon"} apiIndicator={"coming-soon"} data={upcomingList} />
        )}
      </ScrollView>
    </SafeAreaView>
  )
}
