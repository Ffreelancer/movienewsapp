import { faSearch } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import { useNavigation } from "@react-navigation/core"
import React, { FC, useEffect, useState } from "react"
import { TextInput, View, Text, FlatList, ActivityIndicator } from "react-native"
import { TouchableOpacity } from "react-native-gesture-handler"
import { CastCard } from "../../components/cast-card"
import { CrewMoreCard } from "../../components/crew-more-card"
import { HeaderBar } from "../../components/header-bar"
import { MovieCard } from "../../components/movie-card"
import { SearchMovieCard } from "../../components/search-card"
import { Api } from "../../services/api"
import {
  fetchDiscovery,
  fetchMovieCredit,
  fetchPopular,
  fetchSimilarMovies,
  fetchTopRated,
  fetchUpcoming,
  searchMovieByQuery,
} from "../../services/api/movies.api"
import { palette } from "../../theme/palette"
import { styles } from "./cast-view-all.style"

export const CastViewAllScreen: FC = (props: any) => {
  const [movieId, setMovieId] = useState<string>(
    props.route.params.movieId !== undefined ? props.route.params.movieId : "",
  )
  const navigation = useNavigation()
  const api = new Api()

  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [dataArray, setDataArray] = useState<any[]>([])

  const handleCardPress = (id: any) => {
    navigation.navigate(
      "details" as never,
      {
        movieId: id,
      } as never,
    )
  }

  useEffect(() => {
    setIsLoading(true)
    fetchMovieCredit(api, movieId)
      .then((result: any) => {
        setDataArray([...dataArray, ...result.cast])
        setIsLoading(false)
      })
      .catch((err) => {
        console.log("Error While Retreving Data:", err)
      })
  }, [])

  useEffect(() => {
    return () => {}
  }, [])

  return (
    <View style={styles.mainContainer}>
      <HeaderBar />
      <View style={styles.dataHolder}>
        <Text style={styles.titleStyle}>{"Cast & Crew"}</Text>

        {isLoading ? (
          <View style={styles.loaderHolder}>
            <ActivityIndicator size={"large"} color={palette.main} />
          </View>
        ) : (
          <FlatList
            style={{ flexGrow: 1, marginTop: 15 }}
            numColumns={2}
            data={dataArray}
            contentContainerStyle={{ justifyContent: "space-between", alignItems: "center" }}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => <CrewMoreCard key={index} data={item} />}
          />
        )}
      </View>
    </View>
  )
}
