import { Dimensions, StyleSheet } from "react-native"
import { palette } from "../../theme/palette"

const screenWidth = Dimensions.get("screen").width
const screenHeight = Dimensions.get("screen").height

export const styles = StyleSheet.create({
  mainContainer: {
    display: "flex",
    backgroundColor: palette.mainDarker,
    flexGrow: 1,
  },
  dataHolder: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
  },
  titleStyle: {
    color: palette.white,
    fontSize: 24,
    marginBottom: 0,
    paddingHorizontal: 15,
  },
  loaderHolder: {
    display: "flex",
    width: "100%",
    height: screenHeight,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
})
