import { faSearch } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import { useNavigation } from "@react-navigation/core"
import React, { FC, useEffect, useState } from "react"
import { TextInput, View, Text, FlatList, ActivityIndicator } from "react-native"
import { TouchableOpacity } from "react-native-gesture-handler"
import { HeaderBar } from "../../components/header-bar"
import { MovieCard } from "../../components/movie-card"
import { SearchMovieCard } from "../../components/search-card"
import { Api } from "../../services/api"
import {
  fetchDiscovery,
  fetchPopular,
  fetchSimilarMovies,
  fetchTopRated,
  fetchUpcoming,
  searchMovieByQuery,
} from "../../services/api/movies.api"
import { palette } from "../../theme/palette"
import { styles } from "./movie-view-all.style"

export const MovieViewAllScreen: FC = (props: any) => {
  const [titleValue, setTitleValue] = useState<string>(props.route.params.title)
  const [apiIndicator, setApiIndicator] = useState<string>(props.route.params.apiInd)
  const [movieId, setMovieId] = useState<string>(
    props.route.params.movieId !== undefined ? props.route.params.movieId : "",
  )
  const navigation = useNavigation()
  const api = new Api()
  const [pageIndex, setPageIndex] = useState<number>(1)
  const [isFetching, setIsFetching] = useState<boolean>(false)
  const [isDoneFetching, setIsDoneFetching] = useState<boolean>(false)

  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [dataArray, setDataArray] = useState<any[]>([])

  const handleCardPress = (id: any) => {
    navigation.navigate(
      "details" as never,
      {
        movieId: id,
      } as never,
    )
  }

  const handleSelectingAPI = (value: any, page: any) => {
    switch (value) {
      case "discovery":
        return fetchDiscovery(api, page)

        break
      case "coming-soon":
        return fetchUpcoming(api, page)
        break
      case "popular":
        return fetchPopular(api, page)
        break
      case "top-rated":
        return fetchTopRated(api, page)
        break
      case "similar":
        return fetchSimilarMovies(api, movieId, page)
      default:
        return fetchDiscovery(api, page)
    }
  }

  const handleFetchMore = () => {
    if (isFetching !== true) {
      if (isDoneFetching === false) {
        console.log("Started Searching")
        setIsFetching(true)
        handleSelectingAPI(apiIndicator, pageIndex + 1)
          .then((result: any) => {
            if (result.results.length === 0) {
              setIsDoneFetching(true)
              setIsFetching(false)
            } else {
              setPageIndex(pageIndex + 1)
              setDataArray([...dataArray, ...result.results])
              setIsFetching(false)
            }
          })
          .catch((err) => {
            console.log("Error While Fetching More Data:", err)
            setIsFetching(false)
          })
      } else {
        console.log("No More Data")
        setIsFetching(false)
      }
    }
  }

  useEffect(() => {
    setIsLoading(true)
    handleSelectingAPI(apiIndicator, pageIndex)
      .then((result: any) => {
        setDataArray([...dataArray, ...result.results])
        setIsLoading(false)
      })
      .catch((err) => {
        console.log("Error While Retreving Data:", err)
      })
  }, [])

  useEffect(() => {
    return () => {}
  }, [])

  return (
    <View style={styles.mainContainer}>
      <HeaderBar />
      <View style={styles.dataHolder}>
        <Text style={styles.titleStyle}>{titleValue}</Text>

        {isLoading ? (
          <View style={styles.loaderHolder}>
            <ActivityIndicator size={"large"} color={palette.main} />
          </View>
        ) : (
          <FlatList
            style={{ flexGrow: 1, marginTop: 15 }}
            numColumns={2}
            data={dataArray}
            contentContainerStyle={{ justifyContent: "space-between", alignItems: "center" }}
            keyExtractor={(item, index) => index.toString()}
            onEndReachedThreshold={0.5}
            onEndReached={() => {
              handleFetchMore()
            }}
            renderItem={({ item, index }) => (
              <SearchMovieCard
                key={index}
                data={item}
                onPress={() => {
                  props.navigation.push(
                    "details" as never,
                    {
                      movieId: item.id,
                    } as never,
                  )
                }}
              />
            )}
          />
        )}
      </View>
    </View>
  )
}
