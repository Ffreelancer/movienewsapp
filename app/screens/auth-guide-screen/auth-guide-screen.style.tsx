import { Dimensions, StyleSheet } from "react-native"
import { palette } from "../../theme/palette"

const screenWidth = Dimensions.get("screen").width
const screenHeight = Dimensions.get("screen").height

export const styles = StyleSheet.create({
  mainContainer: {
    display: "flex",
    flex: 1,
  },
  overlay: {
    position: "absolute",
    top: 0,
    left: 0,
    height: screenHeight,
    width: screenWidth,
    zIndex: 1,
    backgroundColor: "rgba(0,0,0,.75)",
  },
  bgImage: {
    width: "100%",
    height: "100%",
  },
  authContentHolder: {
    display: "flex",
    flex: 1,
    justifyContent: "flex-end",
    zIndex: 99,
  },
  authContent: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: screenHeight / 3,
    paddingHorizontal: 25,
  },
  appName: {
    fontSize: 24,
    marginBottom: 10,
    fontFamily: "Roboto-Regular",
    color: palette.white,
  },
  appNameSecondPart: {
    color: palette.mainLigher,
  },
  appDescription: {
    fontSize: 14,
    fontFamily: "Roboto-Light",
    color: palette.white,
    textAlign: "center",
  },
  buttonHolder: {
    width: screenWidth,
    display: "flex",
    flexDirection: "column",
    paddingVertical: 25,
    paddingHorizontal: 25,
  },
  loginButton: {
    height: 50,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
    borderRadius: 10,
  },
  guestButton: {
    height: 50,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
    marginTop: 15,
    borderRadius: 10,
    backgroundColor: palette.mainDarker,
  },
  buttonText: {
    color: palette.white,
  },
})
