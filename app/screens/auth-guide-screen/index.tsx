import { useNavigation } from "@react-navigation/core"
import React, { FC, useEffect, useState } from "react"
import { View, Text, ImageBackground } from "react-native"
import { Button } from "../../components"
import { styles } from "./auth-guide-screen.style"

export const AuthGuideScreen: FC = () => {
  const navigation = useNavigation()
  const BG_ARRAY = [
    require("../../../assets/images/bg1.jpg"),
    require("../../../assets/images/bg2.jpg"),
    require("../../../assets/images/bg3.jpeg"),
  ]
  const [bgIndex, setBgIndex] = useState<number>(Math.floor(Math.random() * BG_ARRAY.length))

  /*-------------- Handlers ----------------*/

  const handleGuestButtonClick = () => {
    navigation.navigate("home" as never)
  }

  /*-----------------------------------------*/

  return (
    <View style={styles.mainContainer}>
      <ImageBackground
        resizeMode={"cover"}
        style={styles.bgImage}
        resizeMethod={"auto"}
        source={BG_ARRAY[2]}
      >
        <View style={styles.overlay} />
        <View style={styles.authContentHolder}>
          <View style={styles.authContent}>
            <Text style={styles.appName}>
              {"Movies"}
              <Text style={styles.appNameSecondPart}>{"Tickets"}</Text>
            </Text>
            <Text style={styles.appDescription}>
              {"Book your ticket for your favorite movie anytime from anyplace"}
            </Text>
          </View>
          <View style={styles.buttonHolder}>
            <Button style={styles.loginButton}>
              <Text style={styles.buttonText}>{"Login / Signup"}</Text>
            </Button>
            <Button
              style={styles.guestButton}
              onPress={() => {
                handleGuestButtonClick()
              }}
            >
              <Text style={styles.buttonText}>{"Try as guest!"}</Text>
            </Button>
          </View>
        </View>
      </ImageBackground>
    </View>
  )
}
