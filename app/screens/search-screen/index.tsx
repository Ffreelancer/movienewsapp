import { faSearch } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import { useNavigation } from "@react-navigation/core"
import React, { FC, useEffect, useState } from "react"
import { TextInput, View, Text, FlatList } from "react-native"
import { TouchableOpacity } from "react-native-gesture-handler"
import { HeaderBar } from "../../components/header-bar"
import { MovieCard } from "../../components/movie-card"
import { SearchMovieCard } from "../../components/search-card"
import { Api } from "../../services/api"
import { searchMovieByQuery } from "../../services/api/movies.api"
import { palette } from "../../theme/palette"
import { styles } from "./serach-screen.style"

export const SearchScreen: FC = () => {
  const [searchValue, setSearchValue] = useState<string>("")
  const [searchArray, setSearchArray] = useState<any>([])
  const navigation = useNavigation()
  const api = new Api()
  const [pageIndex, setPageIndex] = useState<number>(1)
  const [isFetching, setIsFetching] = useState<boolean>(false)
  const [isDoneFetching, setIsDoneFetching] = useState<boolean>(false)

  const handleInputChange = (value: any) => {
    setSearchValue(value)
  }

  const handleRunSearch = () => {
    console.log("Search Value", searchValue)
    setPageIndex(1)
    setIsDoneFetching(false)
    searchMovieByQuery(api, searchValue, 1)
      .then((result: any) => {
        setSearchArray(result.results)
      })
      .catch((err) => {
        console.log("Error While Searching for Movies:", err)
      })
  }

  const handleFetchMore = () => {
    if (isFetching !== true) {
      if (isDoneFetching === false) {
        console.log("Started Searching")
        setIsFetching(true)
        searchMovieByQuery(api, searchValue, pageIndex + 1)
          .then((result: any) => {
            if (result.results.length === 0) {
              setIsDoneFetching(true)
              setIsFetching(false)
            } else {
              setPageIndex(pageIndex + 1)
              setSearchArray([...searchArray, ...result.results])
              setIsFetching(false)
            }
          })
          .catch((err) => {
            console.log("Error While Fetching More Data:", err)
            setIsFetching(false)
          })
      } else {
        console.log("No More Data")
        setIsFetching(false)
      }
    }
  }

  const handleCardPress = (id: any) => {
    navigation.navigate(
      "details" as never,
      {
        movieId: id,
      } as never,
    )
  }

  return (
    <View style={styles.mainContainer}>
      <HeaderBar />
      <View style={styles.searchBarHolder}>
        <TextInput
          style={styles.textInputStyle}
          value={searchValue}
          placeholder={"Search Movie...."}
          placeholderTextColor={"rgba(255,255,255,0.5)"}
          onChangeText={(value) => {
            handleInputChange(value)
          }}
        />
        <TouchableOpacity
          style={styles.searchButton}
          onPress={() => {
            handleRunSearch()
          }}
        >
          <FontAwesomeIcon icon={faSearch} color={palette.white} size={20} />
        </TouchableOpacity>
      </View>
      <FlatList
        data={searchArray}
        style={{ flexGrow: 1, marginTop: 15, marginBottom: 200 }}
        numColumns={2}
        contentContainerStyle={{ justifyContent: "space-between", alignItems: "center" }}
        keyExtractor={(item, index) => index.toString()}
        onEndReachedThreshold={0.5}
        onEndReached={() => {
          handleFetchMore()
        }}
        renderItem={({ item, index }) => (
          <SearchMovieCard
            data={item}
            onPress={() => {
              handleCardPress(item.id)
            }}
          />
        )}
      />
    </View>
  )
}
