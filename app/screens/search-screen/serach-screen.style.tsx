import { Dimensions, StyleSheet } from "react-native"
import { palette } from "../../theme/palette"

const screenWidth = Dimensions.get("screen").width
const screenHeight = Dimensions.get("screen").height

export const styles = StyleSheet.create({
  mainContainer: {
    display: "flex",
    backgroundColor: palette.mainDarker,
    flexGrow: 1,
  },
  searchBarHolder: {
    display: "flex",
    height: 60,
    width: screenWidth,
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: 15,
  },
  textInputStyle: {
    height: 50,
    flex: 1,
    paddingHorizontal: 20,
    borderWidth: 1,
    borderColor: "rgba(255,255,255,0.75)",
    color: palette.white,
    borderRadius: 15,
  },
  searchButton: {
    height: 40,
    width: 40,
    marginLeft: 5,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(255,255,255,0.2)",
    borderRadius: 25,
  },
})
