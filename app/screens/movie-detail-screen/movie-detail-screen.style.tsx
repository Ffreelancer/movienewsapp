import { Dimensions, StyleSheet } from "react-native"
import { palette } from "../../theme/palette"

const screenWidth = Dimensions.get("screen").width
const screenHeight = Dimensions.get("screen").height

export const styles = StyleSheet.create({
  mainContainer: {
    display: "flex",
    backgroundColor: palette.mainDarker,
    flexGrow: 1,
  },
  moviePoster: {
    height: screenHeight / 2,
    width: screenWidth,
  },
  headerDetails: {
    height: 50,
    width: screenWidth,
    paddingHorizontal: 15,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    marginTop: 15,
  },
  backButtonStyle: {
    height: 40,
    width: 40,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
    borderRadius: 25,
  },
  heartButtonStyle: {
    height: 40,
    width: 40,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  movieHeader: {
    position: "absolute",
    bottom: 0,
    display: "flex",
    width: "100%",
    height: "30%",
    paddingHorizontal: 15,
  },
  movieTitle: {
    fontSize: 18,
    color: "white",
  },
  movieTitleHolder: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 5,
  },
  movieRatingHolder: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  movieRating: {
    fontSize: 16,
    color: palette.white,
    marginRight: 5,
  },
  movieBreifDescHolder: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 10,
  },
  moviePG: {
    fontSize: 14,
    color: palette.white,
    marginRight: 10,
  },
  movieTime: {
    fontSize: 14,
    color: palette.white,
  },
  movieGenreLabel: {
    height: 30,
    paddingHorizontal: 15,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 5,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 10,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
    backgroundColor: "rgba(255,255,255,0.25)",
  },
  movieGenreText: {
    fontSize: 14,
    color: palette.white,
  },
  movieDetailContainer: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    paddingHorizontal: 15,
  },
  movieDescription: {
    fontSize: 14,
    color: palette.white,
    marginBottom: 15,
  },
  loaderHolder: {
    display: "flex",
    width: "100%",
    height: screenHeight,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  castCrewHolder: {
    display: "flex",
    flexDirection: "column",
    marginBottom: 15,
  },
  castCrewHeader: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  castTitleStyle: {
    fontSize: 18,
    color: palette.white,
  },
  castSeeMoreButton: {
    height: 30,
    width: 30,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  reviewsHolder: {
    display: "flex",
    flexDirection: "column",
    marginBottom: 15,
  },
  reviewsHeader: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  reviewTitleStyle: {
    fontSize: 18,
    color: palette.white,
    marginBottom: 15,
  },
  reviewSeeMoreButton: {
    height: 30,
    width: 30,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },

  similarHolder: {
    display: "flex",
    flexDirection: "column",
    marginBottom: 15,
  },
  similarHeader: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  similarTitleStyle: {
    fontSize: 18,
    color: palette.white,
    marginBottom: 15,
  },
  similarSeeMoreButton: {
    height: 30,
    width: 30,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  similarLoadingHolder: {
    display: "flex",
    width: "100%",
    height: 100,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
})
