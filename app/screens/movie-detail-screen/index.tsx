import {
  faAngleLeft,
  faAngleRight,
  faArrowLeft,
  faHeart,
  faStar,
} from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome"
import { useNavigation } from "@react-navigation/core"
import { LinearGradient } from "expo-linear-gradient"
import React, { FC, useEffect, useState } from "react"
import {
  ActivityIndicator,
  FlatList,
  ImageBackground,
  Text,
  TouchableOpacity,
  View,
} from "react-native"
import { ScrollView } from "react-native-gesture-handler"
import { CastCard } from "../../components/cast-card"
import { MovieCard } from "../../components/movie-card"
import { ReviewCard } from "../../components/review-item"
import { Api } from "../../services/api"
import {
  fetchMovieCredit,
  fetchMovieDetails,
  fetchMovieReview,
  fetchSimilarMovies,
  IMAGE_URL,
} from "../../services/api/movies.api"
import { palette } from "../../theme/palette"
import { styles } from "./movie-detail-screen.style"

export const MovieDetailScreen: FC = (props: any) => {
  const navigation = useNavigation()
  const api = new Api()
  const [movieId, setMovieId] = useState<any>(props.route.params.movieId)

  const [castCrew, setCastCrew] = useState<any>(null)
  const [movieDetails, setMovieDetails] = useState<any>(null)
  const [reviewsList, setReviewsList] = useState<any>(null)
  const [similarList, setSimilarList] = useState<any>([])

  const [isCastCrewLoading, setIsCastCrewLoading] = useState<boolean>(true)
  const [isMovieDetailsLoading, setIsMovieDetailsLoading] = useState<boolean>(true)
  const [isReviewLoading, setIsReviewLoading] = useState<boolean>(true)
  const [isSimilarLoading, setIsSimilarLoading] = useState<boolean>(true)
  const [isSimilarLoaded, setIsSimilarLoaded] = useState<boolean>(false)

  useEffect(() => {
    setIsMovieDetailsLoading(true)
    setIsCastCrewLoading(true)
    setIsReviewLoading(true)

    fetchMovieDetails(api, movieId)
      .then((result: any) => {
        setMovieDetails(result)
        setIsMovieDetailsLoading(false)
      })
      .then(() => {
        fetchMovieCredit(api, movieId)
          .then((result: any) => {
            setCastCrew(result)
            setIsCastCrewLoading(false)
          })
          .then(() => {
            fetchMovieReview(api, movieId).then((result: any) => {
              setReviewsList(result.results)
              setIsReviewLoading(false)
            })
          })
          .catch((err) => {
            console.log("Error While Fetching Review:", err)
          })
          .catch((err) => {
            console.log("Error While Fetching Movie Credits:", err)
          })
      })
      .catch((err) => {
        console.log("Error While Fetching Movie Details:", err)
      })
  }, [])

  useEffect(() => {}, [])

  return (
    <ScrollView
      style={styles.mainContainer}
      onMomentumScrollEnd={(event) => {
        if (isSimilarLoaded !== true) {
          fetchSimilarMovies(api, movieId, 1).then((result: any) => {
            setSimilarList(result)
            setIsSimilarLoading(false)
            setIsSimilarLoaded(true)
          })
        }
      }}
    >
      {isCastCrewLoading || isMovieDetailsLoading ? (
        <View style={styles.loaderHolder}>
          <ActivityIndicator size={"large"} color={palette.main} />
        </View>
      ) : (
        <>
          <ImageBackground
            //source={require("../../../assets/images/moviebg.jpg")}
            source={
              movieDetails.backdrop_path !== null
                ? { uri: `${IMAGE_URL}${movieDetails.backdrop_path}` }
                : require("../../../assets/images/poster-display.png")
            }
            style={styles.moviePoster}
            resizeMethod="resize"
            resizeMode={"cover"}
          >
            <View style={styles.headerDetails}>
              <TouchableOpacity
                onPress={() => {
                  navigation.goBack()
                }}
                style={styles.backButtonStyle}
              >
                <FontAwesomeIcon icon={faAngleLeft} color={palette.white} size={20} />
              </TouchableOpacity>

              <TouchableOpacity style={styles.heartButtonStyle}>
                <FontAwesomeIcon icon={faHeart} color={palette.white} size={20} />
              </TouchableOpacity>
            </View>

            <LinearGradient
              colors={["transparent", palette.mainDarker]}
              locations={[0, 0.5]}
              style={styles.movieHeader}
            >
              <View style={styles.movieTitleHolder}>
                <Text style={styles.movieTitle}>{movieDetails.original_title}</Text>
                <View style={styles.movieRatingHolder}>
                  <Text style={styles.movieRating}>{`${movieDetails.vote_average}/10`}</Text>
                  <FontAwesomeIcon color={"orange"} size={20} icon={faStar} />
                </View>
              </View>
              <View style={styles.movieBreifDescHolder}>
                <Text style={styles.moviePG}>{"PG 13"}</Text>
                <Text style={styles.moviePG}>⸱</Text>
                <Text style={styles.movieTime}>{`${movieDetails.runtime}min`}</Text>
              </View>
              <FlatList
                data={movieDetails.genres}
                style={{ flexGrow: 0 }}
                keyExtractor={(item, index) => index.toString()}
                horizontal
                renderItem={({ item, index }) => (
                  <View style={styles.movieGenreLabel}>
                    <Text style={styles.movieGenreText}>{item.name}</Text>
                  </View>
                )}
              />
            </LinearGradient>
          </ImageBackground>
          <View style={styles.movieDetailContainer}>
            <Text style={styles.movieDescription}>{movieDetails.overview}</Text>
            <View style={styles.castCrewHolder}>
              <View style={styles.castCrewHeader}>
                <Text style={styles.castTitleStyle}>{"Cast & Crew"}</Text>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate(
                      "castViewMore" as never,
                      {
                        movieId: movieId,
                      } as never,
                    )
                  }}
                  style={styles.castSeeMoreButton}
                >
                  <FontAwesomeIcon icon={faAngleRight} color={palette.white} size={20} />
                </TouchableOpacity>
              </View>
              <FlatList
                data={castCrew?.cast.slice(0, 10)}
                horizontal
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => <CastCard data={item} />}
              />
            </View>

            {/*------------- Reviews Section ------------------*/}

            <View style={styles.reviewsHolder}>
              <View style={styles.reviewsHeader}>
                <Text style={styles.reviewTitleStyle}>{"Reviews"}</Text>
                <TouchableOpacity
                  onPress={() => {
                    console.log("Hello")
                  }}
                  style={styles.reviewSeeMoreButton}
                >
                  <FontAwesomeIcon icon={faAngleRight} color={palette.white} size={20} />
                </TouchableOpacity>
              </View>
              <FlatList
                data={reviewsList?.slice(0, 2)}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => <ReviewCard data={item} />}
              />
            </View>

            {/*------------- Similar Section ------------------*/}

            <View style={styles.similarHolder}>
              <View style={styles.similarHeader}>
                <Text style={styles.similarTitleStyle}>{"Similar Movies"}</Text>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate(
                      "viewMore" as never,
                      {
                        title: "Similar Movies",
                        apiInd: "similar",
                        movieId: movieId,
                      } as never,
                    )
                  }}
                  style={styles.similarSeeMoreButton}
                >
                  <FontAwesomeIcon icon={faAngleRight} color={palette.white} size={20} />
                </TouchableOpacity>
              </View>
              {isSimilarLoading ? (
                <View style={styles.similarLoadingHolder}>
                  <ActivityIndicator size={"large"} color={palette.main} />
                </View>
              ) : (
                <FlatList
                  data={similarList?.results.slice(0, 10)}
                  horizontal
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({ item, index }) => (
                    <MovieCard
                      data={item}
                      onPress={() => {
                        props.navigation.push(
                          "details" as never,
                          {
                            movieId: item.id,
                          } as never,
                        )
                      }}
                    />
                  )}
                />
              )}
            </View>
          </View>
        </>
      )}
    </ScrollView>
  )
}
